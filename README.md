# VN Reversing

It so happens that I enjoy reversing VN engines and fileformats more than I do playing VNs.  
So here I'll keep notes, OllyDbg/x64dbg databases, snippets, ODBGScripts, whatever I produce that helps me in the process.  
The (more or less) finished tools regarding a game (such as scripts to unpack an archive or decrypt image files) will be kept inside each game's directory as GIT submodules.