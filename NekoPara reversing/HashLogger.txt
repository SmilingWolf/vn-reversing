var len
var name
var hash
var hashaddr

mov len, 0
gmi eip, CODEBASE

findloop:
// Not too good of a pattern, but whatever. Just loop a number of times until we find what we need.
find $RESULT, #558BECFF75148B550CFF75108B4D08E8????????83C4085DC21000#
inc $RESULT
inc len
cmp len, 5
jle findloop

dec $RESULT
bp $RESULT
run
bc eip

// The script is incomplete, so just pause, set a BP where needed manually, then continue.
pause

loop:
mov len, edi
mul len, 2
mov name, [esi], len
mov hashaddr, edx
sto
mov hash, [hashaddr], 10
buf hash
eval "filename: {name}; hash: {hash}"
log $RESULT, ""
wrta "hashes.txt", $RESULT
run
jmp loop