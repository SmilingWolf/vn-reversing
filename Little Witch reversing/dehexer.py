#!/usr/bin/env python2
import sys
import binascii

fo = open(sys.argv[1], 'r')
for line in fo:
	str = line.rstrip('\n')
	if str.startswith('$HEX['):
		str = str[5:]
		str = str[:-1]
		print binascii.unhexlify(str)
	else:
		print str
fo.close
